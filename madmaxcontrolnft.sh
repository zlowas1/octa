#!/bin/bash
export PATH=$PATH:/usr/local/bin
SERVICE="chia_plot"
TMP_FOLDER="/root/temp/"
PLOT_FOLDER="/root/plot/"
PLOTER_FOLDER="/root/chia-plotter"
#pool contract address chia
PCA="xch1jn2mr6pwflcr7vhxvw8wx9myenlqlwwlsz2ajjskzl8wu2tjsr5sup6qsk"
#farmer pool key chia
FPK="b8043fb29ab00a63b698ec5bd9773a955e04d61e6c56059fe91ed6cb29a4799ee6527cafc43fcec4df2c9429093f2632"


ALLOWED_PLOT_COUNT="7" #plot folderında tasıma için beklemesine izin verilen max plot sayısı

SERVICE_PID=$(pgrep -x "$SERVICE")
PLOT_COUNT=$(find "$PLOT_FOLDER" -maxdepth 1 -type f -name '*.plot' | wc -l)
TMP_COUNT=$(find "$TMP_FOLDER" -maxdepth 1 -type f -name '*.tmp' | wc -l)

if [ ! -z $SERVICE_PID ]
then
    echo "$SERVICE çalışıyor"
    SERVICE_STATUS=$(ps -o s= -p $SERVICE_PID)
    echo "$SERVICE status: $SERVICE_STATUS"
    echo "plot sayısı: $PLOT_COUNT"
    if [ $SERVICE_STATUS == "S" ]
    then
        if [ $PLOT_COUNT -gt $ALLOWED_PLOT_COUNT ]
        then
            #servisi donduralım
            kill -SIGSTOP $SERVICE_PID
            echo "$SERVICE donduruldu"

        else
            #servis çalışmaya devam etsin
            echo "$SERVICE çalışmaya devam ediyor"
        fi
    else
        if [ $SERVICE_STATUS == "T" ]
        then
            if [ $PLOT_COUNT -gt $ALLOWED_PLOT_COUNT ]
            then
                #izin verilenden fazla plot var
                echo "$SERVICE beklemeye devam ediyor"
            else
                #servisi devam ettirelim
                kill -SIGCONT $SERVICE_PID
                echo "$SERVICE devam ettiriliyor"
            fi
        fi
    fi
else
    echo "$SERVICE çalışmıyor"
    if [ $TMP_COUNT -gt 1 ] ; then
        #tempte dosyalar var önceki yarım kalan plottan, silelim
        echo "$TMP_FOLDER içi boşaltılıyor"
        rm "$TMP_FOLDER"*
    else
        echo "temp folder boşaltıldı"
    fi

    if [ $PLOT_COUNT -gt $ALLOWED_PLOT_COUNT ] ; then
        #zaten izin verdiğimiz kadar plot var, bekleyelim biraz boşalsın buralar
        echo "$ALLOWED_PLOT_COUNT plot var, daha calistirmiyoruz" 
    else
        #izin verilenden az plot var
        #plotter dizinine gidelim
        cd $PLOTER_FOLDER
        # minerı çalıştıralım
        echo "madmax screen ile başlatılıyor"
        screen -dmS madmax ./build/$SERVICE -n -1 -r 12 -u 256 -t $TMP_FOLDER -d $PLOT_FOLDER -c $PCA -f $FPK
    fi
fi
