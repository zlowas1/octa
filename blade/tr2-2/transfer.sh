#!/bin/bash
echo "Basladi"
while [ true ]
do
  for file in *
  do
    if [[ $file == *.plot ]]; then
      index=$((`rand -M 3599` + 1))
      acc="$index.json"
      echo "$file,$index"
      gclone move /home/mnt/pw/upload2/ octa1x: --drive-chunk-size 1024M --no-traverse --drive-upload-cutoff 1000T --ignore-existing --drive-stop-on-upload-limit --transfers 3 --drive-service-account-file /root/accounts/"$acc"  --include "/*.plot" -P
      sleep 5
    fi
  done
  sleep 10
done

